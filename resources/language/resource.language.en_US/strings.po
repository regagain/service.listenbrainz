# Kodi Media Center language file
# Addon Name: ListenBrainz
# Addon id: service.listenbrainz
# Addon Provider: Freso
msgid ""
msgstr ""
"Project-Id-Version: XBMC Addons\n"
"Report-Msgid-Bugs-To: alanwww1@xbmc.org\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Frederik “Freso” S. Olesen\n"
"Language-Team: English (US) (http://www.transifex.com/projects/p/xbmc-addons/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgctxt "#30000"
msgid "General"
msgstr ""

msgctxt "#30001"
msgid "Submit tracks to ListenBrainz"
msgstr "Submit tracks to ListenBrainz"

msgctxt "#30002"
msgid "Submit music videos to ListenBrainz"
msgstr "Submit music videos to ListenBrainz"

msgctxt "#30003"
msgid "Submit streaming radio to ListenBrainz"
msgstr "Submit streaming radio to ListenBrainz"

msgctxt "#30004"
msgid "ListenBrainz username"
msgstr "ListenBrainz username"

msgctxt "#30005"
msgid "ListenBrainz token"
msgstr "ListenBrainz token"

msgctxt "#30006"
msgid "ListenBrainz server"
msgstr "ListenBrainz server"

msgctxt "#32011"
msgid "ListenBrainz"
msgstr "ListenBrainz"

msgctxt "#32027"
msgid "No token provided"
msgstr "No token provided"

msgctxt "#32028"
msgid "It doesn’t seem like your specified ListenBrainz server is responding, this might be an issue on the server. If it persists, check if you’ve entered the correct address in your settings."
msgstr "It doesn’t seem like your specified ListenBrainz server is responding, this might be an issue on the server. If it persists, check if you’ve entered the correct address in your settings."
